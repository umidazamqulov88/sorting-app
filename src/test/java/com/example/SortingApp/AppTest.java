package com.example.SortingApp;

import static org.junit.Assert.assertArrayEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class AppTest {
	private String[] input;
	private int[] expected;

	public AppTest(String[] input, int[] expected) {
		this.input = input;
		this.expected = expected;
	}

	@Parameterized.Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][]{
				{new String[]{"3", "1", "2"}, new int[]{1, 2, 3}},
				{new String[]{"5", "4", "3", "2", "1"}, new int[]{1, 2, 3, 4, 5}},
				{new String[]{}, new int[]{}},
				{new String[]{"42"}, new int[]{42}},
				{new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}}, // More than ten arguments
		});
	}

	@Test
	public void testSortNumbers() {
		int[] inputNumbers = Arrays.stream(input)
				.mapToInt(Integer::parseInt)
				.toArray();

		Arrays.sort(inputNumbers);

		assertArrayEquals(expected, inputNumbers);
	}
}
