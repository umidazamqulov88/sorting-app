package com.example.SortingApp;

import java.util.Arrays;

public class App {
	public static void main(String[] args) {
		int[] numbers = Arrays.stream(args)
				.mapToInt(Integer::parseInt)
				.toArray();

		Arrays.sort(numbers);

		System.out.println("Sorted Numbers: " + Arrays.toString(numbers));
	}
}
